package com.example.tourism.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.UUID;

public class ImageUtil {


    /**
     * 输入图片的路径，转换为Base64字符串格式返回
     * @param imagePath
     * @return
     * @throws IOException
     */
    public static String convertImageToBase64(String imagePath) {
        try {
            File imageFile = new File(imagePath);

            String fileType = getFileExtension(imageFile.getName());

            byte[] imageBytes = Files.readAllBytes(imageFile.toPath());
            byte[] base64Bytes = Base64.getEncoder().encode(imageBytes);
            return "data:" + fileType + ";base64," + new String(base64Bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    private static String getFileExtension(String fileName) {  //根据名字获得 后缀 ，即格式
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return null;
        }
    }

    /**
     * 传入图片路径进行删除，根据是否删除成功返回bool值
     * @param path
     * @return
     */
    public static boolean deleteFile(String path){
        File imageFile = new File(path);
        // 检查文件是否存在并且是一个文件
        if (imageFile.exists() && imageFile.isFile()) {
            // 尝试删除文件
            return imageFile.delete();
        } else {
            return false;
        }
    }

    /**
     * @param file 图片文件
     * @param folderPath 图片的文件夹路径
     * @return 存入的文件的名字
     * @throws IOException
     */
    public static String fileUpLoad(MultipartFile file , String folderPath) throws IOException {
        String fname = file.getOriginalFilename();    //获取原始的文件名
        //用UUID作为新文件名，扩展名不变  避免同名覆盖
        String newName = UUID.randomUUID()+ "." + fname.substring(fname.lastIndexOf(".")+1);
        File dest_file = new File( folderPath + newName );
        file.transferTo(dest_file);
        return newName;
    }
}
