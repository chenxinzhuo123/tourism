package com.example.tourism.utils.API;

import java.util.LinkedHashMap;
import java.util.Map;


//步行的api，分为自行车和电动车
//批量算路算法，给百度API发送数据，进行计算，接收json数据
//起终点个数之积不能超过100
//批量算路目前支持驾车、摩托车、骑行（电动车/自行车）、步行。
//根据起点和终点，批量计算路线的距离和耗时
public class walk_API implements requestGet_API {
    private String AK ;
    private String URL = "https://api.map.baidu.com/routematrix/v2/walking?";
    private String origins;  //起点，格式为：纬度,经度|纬度,经度
    private String destinations;  //目标终点，格式为：纬度,经度|纬度,经度
    private Map params ;    //    骑行类型选择，可选值如下：可选值：0 普通自行车 1 电动自行车
    public walk_API(String origins , String destinations , String AK){
        params = new LinkedHashMap<String, String>();
        this.origins = origins;
        this.destinations = destinations;
        this.AK = AK;
    }
    public String getResponse() throws Exception {
        params.put("origins", origins);
        params.put("destinations", destinations);
        params.put("ak", AK);
        String ans = requestGetAK(URL, params);
        return ans.equals("wrong")?null:ans;
    }

}
