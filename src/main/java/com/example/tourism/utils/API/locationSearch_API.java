package com.example.tourism.utils.API;

import java.util.LinkedHashMap;
import java.util.Map;

public class locationSearch_API implements requestGet_API{
    private String URL = "https://api.map.baidu.com/place/v2/suggestion?";

    private String AK ;  // AK密钥
    private Map params; //调用集合的参数
    private String query; //查询的 关键字
    private String region; //所在城市，只能是规定的
    private String city_limit; //查询结果是否限制在所在城市，取值为"true"，仅返回region中指定城市检索结果
    private Object location; //所在的经纬坐标，先 纬度 后 经度
    private String output; //返回值格式	可选值：json、xml

    /**
     * 地点检索API
     * @param Ak 密钥
     * @param query 关键字
     * @param region 所在城市，不能加 市
     * @param location 当前地点
     */
    public locationSearch_API(String Ak, String query, String region, Object location){
        this.params = new LinkedHashMap<String, String>();
        this.AK = Ak;
        this.query = query;
        this.region = region;
        this.location = location;
        city_limit= "true";
        output = "json";
    }
    public String getResponse() throws Exception {
        params.put("query", query);
        params.put("region", region);
        params.put("city_limit", city_limit);
        params.put("output",output);
        params.put("ak", AK);
        params.put("location", location);
        String ans = requestGetAK(URL, params);
        return ans.equals("wrong")?null:ans;
    }
}
