package com.example.tourism.utils.API;


import java.util.LinkedHashMap;
import java.util.Map;

//驾车的api
//批量算路算法，给百度API发送数据，进行计算，接收json数据
//起终点个数之积不能超过100
//批量算路目前支持驾车、摩托车、骑行（电动车/自行车）、步行。
//根据起点和终点，批量计算路线的距离和耗时

public class drive_API implements requestGet_API {

    private String AK ;
    private String URL = "https://api.map.baidu.com/routematrix/v2/driving?";
    private String origins;  //起点，格式为：纬度,经度|纬度,经度
    private String destinations;  //目标终点，格式为：纬度,经度|纬度,经度
    private String tactics;  //偏好设置
    private Map params ;
//    驾车偏好选择，可选值如下：
//            10： 不走高速；
//            11：常规路线，即多数用户常走的一条经验路线，满足大多数场景需求，是较推荐的一个策略
//            12： 距离较短（考虑路况）：即距离相对较短的一条路线，但并不一定是一条优质路线。计算耗时时，考虑路况对耗时的影响；
//            13： 距离较短（不考虑路况）：路线同以上，但计算耗时时，不考虑路况对耗时的影响，可理解为在路况完全通畅时预计耗时。
//            注：除13外，其他偏好的耗时计算都考虑实时路况
    public drive_API(String origins , String destinations , String tactics, String AK){
        params = new LinkedHashMap<String, String>();
        this.origins = origins;
        this.destinations = destinations;
        this.tactics = tactics;
        this.AK = AK;
    }
    public String getResponse() throws Exception {
        params.put("origins", origins);
        params.put("destinations", destinations);
        params.put("tactics",tactics);
        params.put("ak", AK);
        String ans = requestGetAK(URL, params);
        return ans.equals("wrong")?null:ans;
    }



}
