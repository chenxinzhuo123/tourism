package com.example.tourism.Service;

import com.example.tourism.bean.scenicspot.scenicspot;
import com.example.tourism.utils.routing;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class routeService {
    @Value("${tourism.app.ak}")
    private static String AK ;

    private routing r ;
    private int cnt;
    /**
     * 根据传入的景点列表，获取所有的坐标字符串，用于计算行程
     * @param spotsList
     * @return
     */
    private String getLonAndLatStr(List<scenicspot> spotsList){
        StringBuilder str = new StringBuilder();
        for (scenicspot scenicspot : spotsList) {
            str.append(scenicspot.getLon_lat());
            str.append('|');
        }
        return str.substring(0,str.length()-1);
    }
    public int getCnt(){
        return this.cnt;
    }
//    public void run(List<scenicspot> spotsList , String ridingPrefer , int timeLimit ,boolean timePrefer) throws Exception {
//        spotsList.add(new scenicspot());  // 暂存一个，用于后面计算，无实际意义
//        cnt = spotsList.size();
//        String LonAndLatStr ;  //景点坐标字符串
//        int timeCalAfter = 0, sumTime = getInnerTime(spotsList);  //TSP算法后的总时间 和 在景点内的游览花费的总时间
//        do{
//            spotsList.remove(spotsList.size()-1);  //因为时间溢出，故逐渐取出最后的景点
//            LonAndLatStr = getLonAndLatStr(spotsList); //得到景点坐标
//            this.r = new routing(LonAndLatStr,"13",ridingPrefer,this.AK,spotsList.size());
//            this.r.run();
//            sumTime -= spotsList.remove(spotsList.size()-1).getVisit_time();
//            timeCalAfter = timePrefer?this.r.getLen_time() + sumTime:this.r.getDis_time();
//        }while(timeCalAfter>timeLimit);
//    }
    //若是下面得到run方法报错，请用上面的，下面的是优化时间后的版本，上面的版本可能也有问题，有问题找zqr
    public void run(List<scenicspot> spotsList , String ridingPrefer , int Limit ,boolean timePrefer) throws Exception {
        this.cnt = spotsList.size();
        String LonAndLatStr ;  //景点坐标字符串
        int CalAfter = 0 , sumTime = getInnerTime(spotsList);  //TSP算法后的总时间或总路程 和 在景点内的游览花费的总时间
        LonAndLatStr = getLonAndLatStr(spotsList); //得到景点坐标
        this.r = new routing(LonAndLatStr,"13",ridingPrefer,this.AK,cnt);
        this.r.run();
        CalAfter = timePrefer?this.r.getLen_time() + sumTime:this.r.getDis_time();
        while(CalAfter>Limit){
            this.cnt--;
            if(this.cnt==0){
                break;
            }
            this.r.runAfterAPI();

            //随着条件的不满足，内部游览时间逐渐减去最后一个景点
            sumTime -= spotsList.remove(cnt-1).getVisit_time();
            CalAfter = timePrefer?this.r.getLen_time() + sumTime:this.r.getDis_time();
        }
    }
    private int getInnerTime(List<scenicspot> spotsList){
        int sum=0;
        for(scenicspot s : spotsList){
            sum += s.getVisit_time();
        }
        return sum * 60; //因为数据库中存储的是分钟，需要转换为秒
    }
    //以下是获取各种数据
    public int[][] getDriveDistance() {
        return this.r.getDriveDistance();
    }
    public int[][] getDriveTime() {
        return this.r.getDriveTime();
    }
    public int[][] getRideDistance() {
        return this.r.getRideDistance();
    }
    public int[][] getRideTime() {
        return this.r.getRideTime();
    }
    public int[][] getWalkDistance() {
        return this.r.getWalkDistance();
    }
    public int[][] getWalkTime() {
        return this.r.getWalkTime();
    }
    public List<Integer> getTourIdList(boolean timePrefer) {
        return timePrefer?this.r.getTour_time():this.r.getTour_dis();
    }
    public List<Integer> getTourTypeList(boolean timePrefer) {
        return timePrefer?this.r.getMode_time():this.r.getMode_dis();
    }
}
