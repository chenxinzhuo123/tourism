package com.example.tourism.Controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.tourism.Mapper.route.route_comment_imageMapper;
import com.example.tourism.Mapper.route.user_route_ratingMapper;
import com.example.tourism.bean.route.route;
import com.example.tourism.bean.route.routeComment;
import com.example.tourism.bean.route.route_comment_image;
import com.example.tourism.bean.route.user_route_rating;
import com.example.tourism.bean.scenicspot.scenicSpotComment;
import com.example.tourism.bean.scenicspot.scenicspot_comment_image;
import com.example.tourism.utils.ImageUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class user_route_rating_commentController {
    @Autowired
    private user_route_ratingMapper userRouteRatingMapper;
    @Autowired
    private route_comment_imageMapper routeCommentImageMapper;
    @Value("${tourism.app.user_image}")
    private String userImageLocation ;  //存放用户头像图片
    @Value("${tourism.app.route_comment_image}")
    private String routeCommentImageLocation; //存放评论的图片

    /**
     * 根据评论id返回所有的评论信息
     * @param id 评论的id值
     * @return 返回值包括评论列表，和对应的图片列表；routeCommentList[0] 对应 imagesList[0] ；而imagesList[0]又是一个列表，根据该评论是否附加了图片分为含有base64的列表和空列表
     */
    @GetMapping("/routeComment/{id}")  //根据路线id值找评论
    public Map<String,Object> getRouteCommentById(@PathVariable long id){
        Map<String,Object> map = new HashMap<>();  //返回的数据
        List<routeComment> routeCommentList;  //景点评论List
        List<List<String>> imagesList = new ArrayList<>();  //评论的图片，第一层是图片List，第二层是图片，即第一层List包含多个图片
        routeCommentList = userRouteRatingMapper.getRouteCommentByRouteId(id);  //查询 用户名称 用户id 用户头像 评论id 评论内容 评论得分 评论时间 封装在scenicSpotComment类中
        for(routeComment routeComment : routeCommentList){
            if(routeComment.getImage()!=null)  //如果image字段为空则不转换为base64
                routeComment.setImage(ImageUtil.convertImageToBase64(userImageLocation + routeComment.getImage()));
            List<String> images = new ArrayList<>();
            QueryWrapper<route_comment_image> queryWrapperOfImages = new QueryWrapper<>();
            queryWrapperOfImages.eq("comment_ID",routeComment.getCommentID());
            List<route_comment_image> routeCommentImageList = routeCommentImageMapper.selectList(queryWrapperOfImages);
            for(route_comment_image routeCommentImage : routeCommentImageList){
                images.add(ImageUtil.convertImageToBase64(routeCommentImageLocation + routeCommentImage.getImage()));
            }
            imagesList.add(images);
        }
        map.put("routeCommentList",routeCommentList);
        map.put("imagesList",imagesList);
        return map;
    }

    /**
     * 插入某路线评论
     * 下面详细介绍RouteCommentJson（JSON格式）的内容
     * id 用户id
     * comment 评论内容 上限480字
     * score 评论的分数 0-5
     * @param RouteId 路线的id值
     * @param RouteCommentJson 路线评论
     * @param CommentImages 评论的图片
     * @throws IOException
     */
    @PostMapping("/routeComment")
    public void insertRouteComment(@RequestParam("RouteId") long RouteId,
                                   @RequestParam("RouteComment") String RouteCommentJson,
                                   @RequestParam("CommentImages") MultipartFile[] CommentImages) throws IOException {
        routeComment routeComment = new ObjectMapper().readValue(RouteCommentJson,routeComment.class);
        user_route_rating userRouteRating = new user_route_rating();
        userRouteRating.setUser_ID(routeComment.getId());  //设置该评论对应的 用户Id
        userRouteRating.setComment(routeComment.getComment());  //设置该评论内容
        userRouteRating.setScore(routeComment.getScore());  //设置该评论的评分
        userRouteRating.setRoutes_ID(RouteId);  //设置路线的id
        userRouteRatingMapper.insert(userRouteRating);  //插入路线评论
        String routeCommentId = userRouteRating.getId();  //获取评论Id
        //上面是插入评论，下面是插入评论的图片
        if(CommentImages!=null&&CommentImages.length!=0) {
            List<route_comment_image> routeCommentImageList = new ArrayList<>();
            for (MultipartFile file : CommentImages) {
                route_comment_image routeCommentImage = new route_comment_image();
                routeCommentImage.setComment_ID(routeCommentId);
                routeCommentImage.setImage(ImageUtil.fileUpLoad(file, routeCommentImageLocation));  //保存图片
                routeCommentImageList.add(routeCommentImage);
            }
            routeCommentImageMapper.batchInsertRouteCommentImages(routeCommentImageList);  //数据库插入图片
        }
    }

    /**
     * 根据评论id删除评论
     * @param id 路线评论的id值
     */
    @DeleteMapping("/routeComment/{id}")
    public void deleteRouteComment(@PathVariable long id){
        QueryWrapper<route_comment_image> queryWrapperOfImages = new QueryWrapper<>();
        queryWrapperOfImages.eq("comment_ID",id);
        List<route_comment_image> routeCommentImageList = routeCommentImageMapper.selectList(queryWrapperOfImages);  //获取对应comment_ID的数据列表
        for(route_comment_image routeCommentImage : routeCommentImageList){
            ImageUtil.deleteFile(routeCommentImageLocation + routeCommentImage.getImage());  //先删除存储的图片
        }
        routeCommentImageMapper.delete(queryWrapperOfImages);  //在删除数据库中存储的图片信息
        userRouteRatingMapper.deleteById(id);  //根据主键id删除评论信息
    }
}
