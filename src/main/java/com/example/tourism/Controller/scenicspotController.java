package com.example.tourism.Controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.tourism.Mapper.scenicspot.scenicspotMapper;
import com.example.tourism.Mapper.scenicspot.scenicspot_imageMapper;
import com.example.tourism.bean.scenicspot.scenicspot;
import com.example.tourism.bean.scenicspot.scenicspot_image;
import com.example.tourism.utils.ImageUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class scenicspotController {  //TODO 单个景点的相关控制器，包含单个景点的CRUD和审核
    @Autowired
    private scenicspotMapper scenicSpotMapper;
    @Autowired
    private scenicspot_imageMapper scenicSpotImageMapper;
    @Value("${tourism.app.scenicspot_image}")
    private String scenicSpotImageLocation ;
    private static final String pass = "通过";
    private static final String fail = "未通过";
    private static final String audit = "待审核";

    /**
     * 提供景点的id即可，例如http://localhost:8080/scenicspot/1
     * @param id 景点的id值
     * @return 封装的MAP，两个字段分别是scenicSpot，景点的数据；scenicSpotImgs，景点图片数据（List，base64）
     */
    @GetMapping("/scenicSpot/{id}") //获取景点相关的信息，不包括评论
    public Map<String, Object> getScenicSpot(@PathVariable long id){
        Map<String,Object> map = new HashMap<>();  //创建Map，用于存储返回的数据
        scenicspot scenicspot = scenicSpotMapper.selectById(id);  //得到该景点的信息
        QueryWrapper<scenicspot_image> queryWrapperOfScenicSpot_image = new QueryWrapper<>();  //创建景点图片的查询条件
        queryWrapperOfScenicSpot_image.eq("attraction_ID",id);  //匹配该景点id的图片
        List<scenicspot_image> scenicspotImageList = scenicSpotImageMapper.selectList(queryWrapperOfScenicSpot_image);
        List<String> scenicspotImageBase64List = new ArrayList<>();
        for(scenicspot_image spotImg : scenicspotImageList){
            String path = scenicSpotImageLocation + spotImg.getImage();
            scenicspotImageBase64List.add(ImageUtil.convertImageToBase64(path));  //将图片转换为Base64格式
        }
        map.put("scenicSpot",scenicspot);  //存入景点数据
        map.put("scenicSpotImgs",scenicspotImageBase64List);  //存入景点图片
        return map;
    }

    /**
     * 插入景点数据，其中scenicSpotJson的数据有
     * name 必须，不超过80字
     * description 非必须，不超过65535字
     * location 必须，详细到 区级单位 即可
     * type 非必须，不超过80字
     * createuser 后期不用写，但权限管理框架还未写，所以前端先默认为 1
     * lon_lat 必须，经纬度，前端需要检查经纬度信息是否符合格式，先纬度后经度，用英文逗号隔开
     * belong 必须，该景点或地方属于哪里，即上一级是什么
     * visit_time 必须，景点预计参观时间，单位：分钟
     * @param scenicSpotJson 景点的数据，JSON
     * @param images 图片列表或数组，可以没有图片，但是前端要发送一个空数组或列表
     * @throws IOException
     */
    @PostMapping("/scenicSpot")  //插入一条景点信息
    public void insertScenicSpot(@RequestParam("scenicSpot") String scenicSpotJson, @RequestParam("scenicSpotImgs")MultipartFile[] images) throws IOException {
        scenicspot scenicspot = new ObjectMapper().readValue(scenicSpotJson, scenicspot.class);  //将json转换为类
        scenicspot.setStatus(audit);
        boolean successInsert = scenicSpotMapper.insert(scenicspot) > 0;
        if(!successInsert){
            return;    //如果插入失败则返回，停止插入
        }
        long id = scenicspot.getId();  //插入数据后获取Id值
        //上面是插入景点数据，下面是插入景点图片数据
        if(images==null||images.length==0)  //如果图片为空则返回，不插入图片数据
            return;
        List<scenicspot_image> scenicSpotImageList = new ArrayList<>();
        for(MultipartFile file : images){
            scenicspot_image scenicspotImage = new scenicspot_image();
            scenicspotImage.setAttraction_ID(id);
            scenicspotImage.setImage(ImageUtil.fileUpLoad(file,scenicSpotImageLocation));
            scenicSpotImageList.add(scenicspotImage);
        }
        scenicSpotImageMapper.batchInsertScenicSpotImages(scenicSpotImageList);
    }

    /**
     * 修改景点数据，其中scenicSpotJson的数据有
     * id 必须
     * name 必须，不超过80字
     * description 非必须，不超过65535字
     * location 必须，详细到 区级单位 即可
     * type 非必须，不超过80字
     * averageRating 必须，是后端给前端的值
     * createuser 后期不用写，但权限管理框架还未写，所以前端先默认为 1
     * createtime 必须，是后端给前端的值
     * lon_lat 必须，经纬度，前端需要检查经纬度信息是否符合格式，先纬度后经度，用英文逗号隔开
     * belong 必须，该景点或地方属于哪里，即上一级是什么
     * visit_time 必须，景点预计参观时间，单位：分钟
     * @param scenicSpotJson
     * @param images
     * @throws IOException
     */
    @PutMapping("/scenicSpot") //修改景点数据
    public void updateScenicSpot(@RequestParam("scenicSpot") String scenicSpotJson, @RequestParam("scenicSpotImgs")MultipartFile[] images) throws IOException {
        scenicspot scenicspot = new ObjectMapper().readValue(scenicSpotJson, scenicspot.class);  //将json转换为类
        scenicspot.setStatus(audit);
        long id = scenicspot.getId(); //获取id值
        scenicSpotMapper.updateById(scenicspot);  //更新景点数据信息

        //删除之前的景点图片
        QueryWrapper<scenicspot_image> queryWrapperOfImages = new QueryWrapper<>();  //建立查询条件
        queryWrapperOfImages.eq("attraction_ID",id);
        List<scenicspot_image> scenicspotImageList = scenicSpotImageMapper.selectList(queryWrapperOfImages);
        for(scenicspot_image image : scenicspotImageList){
            ImageUtil.deleteFile(scenicSpotImageLocation+image.getImage()); //删除之前的图片信息
        }
        scenicSpotImageMapper.delete(queryWrapperOfImages);  //删除景点图片

        //插入新的景点图片
        if(images==null||images.length==0)
            return;
        List<scenicspot_image> scenicSpotImageList = new ArrayList<>();
        for(MultipartFile file : images){
            scenicspot_image scenicspotImage = new scenicspot_image();
            scenicspotImage.setAttraction_ID(id);
            scenicspotImage.setImage(ImageUtil.fileUpLoad(file,scenicSpotImageLocation));
            scenicSpotImageList.add(scenicspotImage);
        }
        scenicSpotImageMapper.batchInsertScenicSpotImages(scenicSpotImageList);
    }

    /**
     * 提供景点的id即可，例如http://localhost:8080/scenicspot/1
     * @param id 删除的景点id值
     */
    @DeleteMapping("/scenicSpot/{id}") //删除景点数据
    public void deleteScenicSpot(@PathVariable long id){
        QueryWrapper<scenicspot_image> queryWrapperOfImages = new QueryWrapper<>();  //建立景点图片查询条件
        queryWrapperOfImages.eq("attraction_ID",id);
        List<scenicspot_image> scenicspotImageList = scenicSpotImageMapper.selectList(queryWrapperOfImages);
        for(scenicspot_image image : scenicspotImageList){
            ImageUtil.deleteFile(scenicSpotImageLocation+image.getImage()); //删除之前的图片信息
        }
        scenicSpotImageMapper.delete(queryWrapperOfImages);  //删除数据库中图片
        scenicSpotMapper.deleteById(id);  //删除景点数据
    }

    /**
     * 根据用户id设置 类型 为 通过
     * @param id scenicSpot的id值
     */
    @PutMapping("/scenicSpot/pass/{id}")
    public void updateScenicSpotForPass(@PathVariable long id){
        scenicSpotMapper.updateScenicSpotStatus(id,pass);
    }

    /**
     * 根据用户id设置 类型 为 通过
     * @param id scenicSpot的id值
     */
    @PutMapping("/scenicSpot/fail/{id}")
    public void updateScenicSpotForFail(@PathVariable long id){
        scenicSpotMapper.updateScenicSpotStatus(id,fail);
    }

    /**
     * 根据用户id设置 类型 为 通过
     * @param id scenicSpot的id值
     */
    @PutMapping("/scenicSpot/audit/{id}")
    public void updateScenicSpotForAudit(@PathVariable long id){
        scenicSpotMapper.updateScenicSpotStatus(id,audit);
    }
}
