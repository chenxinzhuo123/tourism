package com.example.tourism.Controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.tourism.Mapper.route.routeMapper;
import com.example.tourism.Mapper.route.route_scenicspotMapper;
import com.example.tourism.bean.route.route;
import com.example.tourism.bean.route.route_scenicspot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class routesDataController { //TODO 查询批量路线等相关内容的控制器，返回多个路线数据，未使用postman测试，有bug反馈
    @Autowired
    private route_scenicspotMapper routeScenicSpotMapper;
    @Autowired
    private routeMapper routeMapper;
    private static final String pass = "通过";
    private static final String fail = "未通过";
    private static final String audit = "待审核";

    /**
     * 查询所有路线
     * @return 封装的列表，列表的每一项是一个map，route关键字是”路线数据“，scenicSpots是”路线对应的景点“
     */
    @GetMapping("/routesData")
    public List<Map<String,Object>> getRoutesData(){
        List<Map<String,Object>> routesDataList = new ArrayList<>();  //封装返回值
        List<route> routeList = routeMapper.selectList(null); //查询所有路线
        for(route route : routeList){
            Map<String,Object> map = new HashMap<>();
            QueryWrapper<route_scenicspot> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByAsc("cnt");   //按照cnt的升序进行排列
            queryWrapper.eq("routes_ID",route.getId());  //路线id值设置
            List<route_scenicspot> routeScenicspotList = routeScenicSpotMapper.selectList(queryWrapper);
            map.put("route",route);  //放入路线数据
            map.put("scenicSpots",routeScenicspotList);  //放入景点数据
            routesDataList.add(map);
        }
        return routesDataList;
    }
    /**
     * 查询所有 状态为 通过 的路线
     * @return 封装的列表，列表的每一项是一个map，route关键字是”路线数据“，scenicSpots是”路线对应的景点“
     */
    @GetMapping("/routesDataOfPass")
    public List<Map<String,Object>> getRoutesDataOfPass(){
        List<Map<String,Object>> routesDataList = new ArrayList<>();  //封装返回值
        List<route> routeList = routeMapper.selectList(new QueryWrapper<route>().eq("status",pass)); //查询状态为通过的所有路线
        for(route route : routeList){
            Map<String,Object> map = new HashMap<>();
            QueryWrapper<route_scenicspot> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByAsc("cnt");   //按照cnt的升序进行排列
            queryWrapper.eq("routes_ID",route.getId());  //路线id值设置
            List<route_scenicspot> routeScenicspotList = routeScenicSpotMapper.selectList(queryWrapper);
            map.put("route",route);  //放入路线数据
            map.put("scenicSpots",routeScenicspotList);  //放入景点数据
            routesDataList.add(map);
        }
        return routesDataList;
    }
    /**
     * 查询所有 状态为 未通过 的路线
     * @return 封装的列表，列表的每一项是一个map，route关键字是”路线数据“，scenicSpots是”路线对应的景点“
     */
    @GetMapping("/routesDataOfFail")
    public List<Map<String,Object>> getRoutesDataOfFail(){
        List<Map<String,Object>> routesDataList = new ArrayList<>();  //封装返回值
        List<route> routeList = routeMapper.selectList(new QueryWrapper<route>().eq("status",fail)); //查询状态为未通过的所有路线
        for(route route : routeList){
            Map<String,Object> map = new HashMap<>();
            QueryWrapper<route_scenicspot> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByAsc("cnt");   //按照cnt的升序进行排列
            queryWrapper.eq("routes_ID",route.getId());  //路线id值设置
            List<route_scenicspot> routeScenicspotList = routeScenicSpotMapper.selectList(queryWrapper);
            map.put("route",route);  //放入路线数据
            map.put("scenicSpots",routeScenicspotList);  //放入景点数据
            routesDataList.add(map);
        }
        return routesDataList;
    }

    /**
     * 查询所有 状态为 待审核 的路线
     * @return 封装的列表，列表的每一项是一个map，route关键字是”路线数据“，scenicSpots是”路线对应的景点“
     */
    @GetMapping("/routesDataOfAudit")
    public List<Map<String,Object>> getRoutesDataOfAudit(){
        List<Map<String,Object>> routesDataList = new ArrayList<>();  //封装返回值
        List<route> routeList = routeMapper.selectList(new QueryWrapper<route>().eq("status",audit)); //查询状态为待审核的所有路线
        for(route route : routeList){
            Map<String,Object> map = new HashMap<>();
            QueryWrapper<route_scenicspot> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByAsc("cnt");   //按照cnt的升序进行排列
            queryWrapper.eq("routes_ID",route.getId());  //路线id值设置
            List<route_scenicspot> routeScenicspotList = routeScenicSpotMapper.selectList(queryWrapper);
            map.put("route",route);  //放入路线数据
            map.put("scenicSpots",routeScenicspotList);  //放入景点数据
            routesDataList.add(map);
        }
        return routesDataList;
    }

    @PostMapping("/routesData")
    public List<Map<String,Object>> getRoutesDataOfAudit(@RequestParam String keyword){
        List<Map<String,Object>> routesDataList = new ArrayList<>();  //封装返回值
        List<route> routeList = routeMapper.selectList(new QueryWrapper<route>().like("name",keyword)); //查询 名称包含关键字 的所有路线
        for(route route : routeList){
            Map<String,Object> map = new HashMap<>();
            QueryWrapper<route_scenicspot> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByAsc("cnt");   //按照cnt的升序进行排列
            queryWrapper.eq("routes_ID",route.getId());  //路线id值设置
            List<route_scenicspot> routeScenicspotList = routeScenicSpotMapper.selectList(queryWrapper);
            map.put("route",route);  //放入路线数据
            map.put("scenicSpots",routeScenicspotList);  //放入景点数据
            routesDataList.add(map);
        }
        return routesDataList;
    }







}
