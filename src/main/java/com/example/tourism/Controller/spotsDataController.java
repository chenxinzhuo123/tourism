package com.example.tourism.Controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.tourism.Mapper.scenicspot.scenicspotMapper;
import com.example.tourism.Mapper.scenicspot.scenicspot_imageMapper;
import com.example.tourism.bean.scenicspot.scenicspot;
import com.example.tourism.bean.scenicspot.scenicspot_image;
import com.example.tourism.utils.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class spotsDataController {  //TODO 查询批量景点等相关内容的控制器，返回多个景点数据
    @Autowired
    private scenicspotMapper spotsMapper;
    @Autowired
    private scenicspot_imageMapper scenicspotImageMapper;
    @Value("${tourism.app.scenicspot_image}")
    private String scenicSpotImageLocation ;
    private static final String pass = "通过";
    private static final String fail = "未通过";
    private static final String audit = "待审核";

    /**
     * 查询所有景点的信息
     * @return 包括两部分，分别是景点主体部分数据和景点图片数据两个列表，一一对应
     */
    @GetMapping("/spotsData")  //获得所有景点以及图片
    public Map<String, Object> getScenicSpotsData() {
        Map<String, Object> spotsData = new HashMap<>();  //封装在Map里面进行返回
        List<String> ImgBase64List = new ArrayList<>();
        List<scenicspot_image> scenicspot_imageList = scenicspotImageMapper.getSingleImg();
        for(scenicspot_image si : scenicspot_imageList){
            String path = scenicSpotImageLocation + si.getImage();
            String imgBase64 = ImageUtil.convertImageToBase64(path);
            ImgBase64List.add(imgBase64);//获得景点的封面照片，即每个景点只有一个图片，每个图片的格式是base64字符串，可以直接作为src属性值
        }
        // 使用 QueryWrapper 进行条件查询
        QueryWrapper<scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");// 按照 id 升序排列
        spotsData.put("spotsDetails",spotsMapper.selectList(queryWrapper)); //按照queryWrapper查询条件进学校查询
        spotsData.put("spotsImgs",ImgBase64List);
        return spotsData;
    }

    /**
     * 查询 状态为 通过 景点的信息
     * @return 包括两部分，分别是景点主体部分数据和景点图片数据两个列表，一一对应
     */
    @GetMapping("/spotsDataOfPass")  //获得所有景点以及图片
    public Map<String, Object> getScenicSpotsDataOfPass()  {
        Map<String, Object> spotsData = new HashMap<>();  //封装在Map里面进行返回
        List<String> ImgBase64List = new ArrayList<>();

        // 使用 QueryWrapper 进行条件查询
        QueryWrapper<scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");// 按照 id 升序排列
        queryWrapper.like("status",pass); //只查找 状态为 通过 的景点
        List<scenicspot> scenicspotList = spotsMapper.selectList(queryWrapper);
        List<Long> attractionIdList = new ArrayList<>();
        for(scenicspot s : scenicspotList){
            attractionIdList.add(s.getId());
        }
        if(attractionIdList.size()!=0) {
            List<scenicspot_image> scenicspot_imageList = scenicspotImageMapper.getSingleImgByIds(attractionIdList); //查询包含在attractionIdList中的
            for (scenicspot_image si : scenicspot_imageList) {
                String path = scenicSpotImageLocation + si.getImage();
                String imgBase64 = ImageUtil.convertImageToBase64(path);
                ImgBase64List.add(imgBase64);//获得景点的封面照片，即每个景点只有一个图片，每个图片的格式是base64字符串，可以直接作为src属性值
            }
        }
        spotsData.put("spotsDetails",scenicspotList); //按照queryWrapper查询条件进学校查询
        spotsData.put("spotsImgs",ImgBase64List);
        return spotsData;
    }

    /**
     * 查询所有景点的信息 未通过的
     * @return 包括两部分，分别是景点主体部分数据和景点图片数据两个列表，一一对应
     */
    @GetMapping("/spotsDataOfFail")  //获得所有景点以及图片
    public Map<String, Object> getScenicSpotsDataOfFail()  {
        Map<String, Object> spotsData = new HashMap<>();  //封装在Map里面进行返回
        List<String> ImgBase64List = new ArrayList<>();

        // 使用 QueryWrapper 进行条件查询
        QueryWrapper<scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");// 按照 id 升序排列
        queryWrapper.like("status",fail); //只查找 状态为 未通过 的景点
        List<scenicspot> scenicspotList = spotsMapper.selectList(queryWrapper);
        List<Long> attractionIdList = new ArrayList<>();
        for(scenicspot s : scenicspotList){
            attractionIdList.add(s.getId());
        }
        if(attractionIdList.size()!=0) {
            List<scenicspot_image> scenicspot_imageList = scenicspotImageMapper.getSingleImgByIds(attractionIdList); //查询包含在attractionIdList中的
            for (scenicspot_image si : scenicspot_imageList) {
                String path = scenicSpotImageLocation + si.getImage();
                String imgBase64 = ImageUtil.convertImageToBase64(path);
                ImgBase64List.add(imgBase64);//获得景点的封面照片，即每个景点只有一个图片，每个图片的格式是base64字符串，可以直接作为src属性值
            }
        }
        spotsData.put("spotsDetails",scenicspotList); //按照queryWrapper查询条件进学校查询
        spotsData.put("spotsImgs",ImgBase64List);
        return spotsData;
    }

    /**
     * 查询所有景点的信息 待审核的
     * @return 包括两部分，分别是景点主体部分数据和景点图片数据两个列表，一一对应
     */
    @GetMapping("/spotsDataOfAudit")  //获得所有景点以及图片
    public Map<String, Object> getScenicSpotsDataOfAudit() {
        Map<String, Object> spotsData = new HashMap<>();  //封装在Map里面进行返回
        List<String> ImgBase64List = new ArrayList<>();

        // 使用 QueryWrapper 进行条件查询
        QueryWrapper<scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");// 按照 id 升序排列
        queryWrapper.like("status",audit); //只查找 状态为 待审核 的景点
        List<scenicspot> scenicspotList = spotsMapper.selectList(queryWrapper);
        List<Long> attractionIdList = new ArrayList<>();
        for(scenicspot s : scenicspotList){
            attractionIdList.add(s.getId());
        }
        if(attractionIdList.size()!=0) {
            List<scenicspot_image> scenicspot_imageList = scenicspotImageMapper.getSingleImgByIds(attractionIdList); //查询包含在attractionIdList中的
            for (scenicspot_image si : scenicspot_imageList) {
                String path = scenicSpotImageLocation + si.getImage();
                String imgBase64 = ImageUtil.convertImageToBase64(path);
                ImgBase64List.add(imgBase64);//获得景点的封面照片，即每个景点只有一个图片，每个图片的格式是base64字符串，可以直接作为src属性值
            }
        }
        spotsData.put("spotsDetails",scenicspotList); //按照queryWrapper查询条件进学校查询
        spotsData.put("spotsImgs",ImgBase64List);
        return spotsData;
    }

    /**
     * 根据关键字查询景点信息，只包含通过的景点
     * @param keyword name字段进行粗查询
     * @return 包括两部分，分别是景点主体部分数据和景点图片数据两个列表，一一对应
     */
    @PostMapping("/spotsData") //根据关键字查询景点
    public Map<String, Object> getScenicSpotsDataByKeyword(@RequestParam String keyword)  {
        Map<String, Object> spotsData = new HashMap<>();  //封装在Map里面进行返回
        List<String> ImgBase64List = new ArrayList<>();
        // 使用 QueryWrapper 进行条件查询
        QueryWrapper<scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");// 按照 id 升序排列
        queryWrapper.like("name", keyword); //查询 name 字段中包含keyword的景点信息
        queryWrapper.eq("status",pass); //只查找 状态为 通过 的景点
        List<scenicspot> scenicspotList = spotsMapper.selectList(queryWrapper);

        List<Long> attractionIdList = new ArrayList<>();
        for(scenicspot s : scenicspotList){
            attractionIdList.add(s.getId());
        }
        if(attractionIdList.size()!=0) {
            List<scenicspot_image> scenicspot_imageList = scenicspotImageMapper.getSingleImgByIds(attractionIdList); //查询包含在attractionIdList中的
            for (scenicspot_image si : scenicspot_imageList) {
                String path = scenicSpotImageLocation + si.getImage();
                String imgBase64 = ImageUtil.convertImageToBase64(path);
                ImgBase64List.add(imgBase64);//获得景点的封面照片，即每个景点只有一个图片，每个图片的格式是base64字符串，可以直接作为src属性值
            }
        }

        spotsData.put("spotsDetails",scenicspotList); //按照queryWrapper查询条件进学校查询
        spotsData.put("spotsImgs",ImgBase64List);
        return spotsData;
    }

    /**
     * 查询所有的Belong字段，返回值不重复
     * @return
     */
    @GetMapping("/belongs")
    public List<String> getBelongsFromScenicSpots(){  //得到所有的belong字段，不重复
        return spotsMapper.getBelongs();
    }

    /**
     * 传入keyword关键字
     * @param keyword belong关键字
     * @return 通过的景点的数据，包括两部分，分别是景点主体数据和图片数据两个列表，一一对应
     */
    @PostMapping("/spotsDataRoute") //根据Belong关键字查询景点
    public Map<String, Object> getScenicSpotsDataByBelong(@RequestParam String keyword)  {
        Map<String, Object> spotsData = new HashMap<>();  //封装在Map里面进行返回
        List<String> ImgBase64List = new ArrayList<>();
        // 使用 QueryWrapper 进行条件查询
        QueryWrapper<scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");// 按照 id 升序排列
        queryWrapper.eq("belong", keyword); //查询 belong 字段中等于keyword的景点信息
        queryWrapper.eq("status",pass); //只查找 状态为 通过 的景点
        List<scenicspot> scenicspotList = spotsMapper.selectList(queryWrapper);
        List<Long> attractionIdList = new ArrayList<>();
        for(scenicspot s : scenicspotList){
            attractionIdList.add(s.getId());
        }
        if(attractionIdList.size()!=0) {
            List<scenicspot_image> scenicspot_imageList = scenicspotImageMapper.getSingleImgByIds(attractionIdList); //查询包含在attractionIdList中的
            for (scenicspot_image si : scenicspot_imageList) {
                String path = scenicSpotImageLocation + si.getImage();
                String imgBase64 = ImageUtil.convertImageToBase64(path);
                ImgBase64List.add(imgBase64);//获得景点的封面照片，即每个景点只有一个图片，每个图片的格式是base64字符串，可以直接作为src属性值
            }
        }
        spotsData.put("spotsDetails",scenicspotList); //按照queryWrapper查询条件进学校查询
        spotsData.put("spotsImgs",ImgBase64List);
        return spotsData;
    }
}
