package com.example.tourism.Controller;

import com.example.tourism.Mapper.userMapper;
import com.example.tourism.bean.user;
import com.example.tourism.utils.ImageUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class userController {  //TODO 用户的CRUD相关的控制器，目前不包括登录功能
    @Autowired
    userMapper userMapper;
    @Value("${tourism.app.user_image}")
    private String userImageLocation ;

    /**
     * 获取用户的所有信息
     * @return 所有用户信息，image字段封装的是base64
     */
    @GetMapping("/userList")  //获取所有用户信息
    public List<user> getUserList() {
        // 从数据库中获取数据
       List<user> userList = userMapper.selectList(null);
       for(user user : userList){
           if(user.getImage()==null)  //跳过没有图片的情况
               continue;
           String imagePath = userImageLocation + "\\" + user.getImage();
           String base64Img = ImageUtil.convertImageToBase64(imagePath);
           user.setImage(base64Img); //给前端传数据时，将数据库中存储的 头像图片的名字 改为baseb4
       }
        return userList;
    }

    /**
     * 提供用户的id即可，例如http://localhost:8080/user/1
     * @param id
     * @return 返回用户的数据，用户的头像封装在了user的image字段，格式为base64
     */
    @GetMapping("/user/{id}")  //根据用户id获得用户所有信息
    public user getUserDetail(@PathVariable Long id) {
        user user = userMapper.selectById(id);
        if(user.getImage()!=null) {
            String imagePath = userImageLocation + "\\" + user.getImage();
            String base64Img = ImageUtil.convertImageToBase64(imagePath);
            user.setImage(base64Img);  //给前端传数据时，将数据库中存储的 头像图片的名字 改为baseb4
        }
        return user;
    }

    /**
     * userJson包含的数据有：
     * name必选
     * tel 必选
     * password 必选
     * sex 必选
     * email 必选
     * birth 非必选
     * self_introduction 非必选，长度不超过200字
     * @param userJson user的JSON数据
     * @param image 非必选
     * @throws IOException
     */
    @PostMapping("/user") //新增用户数据
    public void insertUserDetail(@RequestParam("user") String userJson , @RequestParam("image")MultipartFile image)throws IOException{
        user user = new ObjectMapper().readValue(userJson, user.class);
        user.setType("普通用户");
        if(image!=null&&!image.isEmpty())
            user.setImage(ImageUtil.fileUpLoad(image,userImageLocation));  //如果图片不为空，则设置用户信息
        userMapper.insert(user);  //插入用户信息
    }

    /**
     * userJson包含的数据有：
     * id 必选
     * name必选
     * tel 必选
     * password 必选
     * sex 必选
     * email 必选
     * birth 非必选
     * self_introduction 非必选，长度不超过200字
     * @param userJson user的JSON数据
     * @param image 非必选
     * @throws IOException
     */
    @PutMapping("/user")  //修改用户数据
    public void updateUserDetail(@RequestParam("user") String userJson , @RequestParam("image")MultipartFile image) throws IOException {
        user user = userMapper.selectById(new ObjectMapper().readValue(userJson, user.class).getId());
        user.setType("普通用户");
        if(user.getImage()!=null) {
            String imagePath = userImageLocation + "\\" + user.getImage();
            ImageUtil.deleteFile(imagePath); //删除之前的图片
        }
        if(image!=null&&!image.isEmpty())
            user.setImage(ImageUtil.fileUpLoad(image,userImageLocation));  //如果图片不为空，则设置图片信息
        userMapper.updateById(user);  //更新用户信息
    }

    /**
     * 提供用户的id即可，例如http://localhost:8080/user/1
     * @param id
     */
    @DeleteMapping("user/{id}") //删除用户数据
    public void deleteUserDetail(@PathVariable long id){
        user user = userMapper.selectById(id);  //根据id找到用户信息
        if(user.getImage()!=null) {
            String imagePath = userImageLocation + "\\" + user.getImage();
            ImageUtil.deleteFile(imagePath);  //删除用户的图片
        }
        userMapper.deleteById(user);  //删除用户信息，不会删除对应的评论等信息，如果查询则显示 该用户已注销
    }
}
