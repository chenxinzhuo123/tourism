package com.example.tourism.Controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.tourism.Mapper.scenicspot.scenicspot_comment_imageMapper;
import com.example.tourism.Mapper.scenicspot.user_scenicspot_ratingMapper;
import com.example.tourism.bean.scenicspot.scenicSpotComment;
import com.example.tourism.bean.scenicspot.scenicspot_comment_image;
import com.example.tourism.bean.scenicspot.user_scenicspot_rating;
import com.example.tourism.utils.ImageUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@CrossOrigin(origins = "*")
public class user_scenicspot_rating_commentController {  //TODO 用户-景点-评论-得分-评论图片 的 控制器，控制景点的评论以及评论的图片
    @Autowired
    private user_scenicspot_ratingMapper userScenicSpotRatingMapper;
    @Autowired
    private scenicspot_comment_imageMapper scenicSpotCommentImageMapper;
    @Value("${tourism.app.scenicspot_comment_image}")
    private String scenicSpotCommentImageLocation ;   //存放景点评论的图片
    @Value("${tourism.app.user_image}")
    private String userImageLocation ;  //存放头像图片

    /**
     * 根据景点id返回所有的评论信息
     * @param id 景点的id值
     * @return 返回值包括评论列表，和对应的图片列表；scenicSpotCommentList[0] 对应 imagesList[0] ；而imagesList[0]又是一个列表，根据该评论是否附加了图片分为含有base64的列表和空列表
     */
    @GetMapping("/scenicSpotComment/{id}")  //根据景点id值找评论
    public Map<String,Object> getScenicSpotCommentById(@PathVariable long id){
        Map<String,Object> map = new HashMap<>();  //返回的数据
        List<scenicSpotComment> scenicSpotCommentList;  //景点评论List
        List<List<String>> imagesList = new ArrayList<>();  //评论的图片，第一层是图片List，第二层是图片，即第一层List包含多个图片
        scenicSpotCommentList = userScenicSpotRatingMapper.getScenicSpotCommentByScenicSpotId(id);  //查询 用户名称 用户id 用户头像 评论id 评论内容 评论得分 评论时间 封装在scenicSpotComment类中
        for(scenicSpotComment spotComment : scenicSpotCommentList){
            if(spotComment.getImage()!=null)  //如果image字段为空则不转换为base64
                spotComment.setImage(ImageUtil.convertImageToBase64(userImageLocation + spotComment.getImage()));
            List<String> images = new ArrayList<>();
            QueryWrapper<scenicspot_comment_image> queryWrapperOfImages = new QueryWrapper<>();
            queryWrapperOfImages.eq("comment_ID",spotComment.getCommentID());
            List<scenicspot_comment_image> scenicspotCommentImageList = scenicSpotCommentImageMapper.selectList(queryWrapperOfImages);
            for(scenicspot_comment_image scenicspotCommentImage : scenicspotCommentImageList){
                images.add(ImageUtil.convertImageToBase64(scenicSpotCommentImageLocation + scenicspotCommentImage.getImage()));
            }
            imagesList.add(images);
        }
        map.put("scenicSpotCommentList",scenicSpotCommentList);
        map.put("imagesList",imagesList);
        return map;
    }

    /**
     * 插入某景点评论
     * 下面详细介绍ScenicSpotCommentJson（JSON格式）的内容
     * id 用户id
     * comment 评论内容 上限480字
     * score 评论的分数 0-5
     * @param ScenicSpotId 景点id值
     * @param ScenicSpotCommentJson 评论内容，上面详细介绍
     * @param CommentImages 评论的图片，可以没有，但是要发送占位，即空数组
     * @throws IOException
     */
    @PostMapping("/scenicSpotComment")
    public void insertScenicSpotComment(@RequestParam("ScenicSpotId") long ScenicSpotId,
                                        @RequestParam("SpotComment") String ScenicSpotCommentJson,
                                        @RequestParam("CommentImages")MultipartFile[] CommentImages) throws IOException {
        scenicSpotComment scenicSpotComment = new ObjectMapper().readValue(ScenicSpotCommentJson, scenicSpotComment.class);
        user_scenicspot_rating userScenicSpotRating = new user_scenicspot_rating();
        userScenicSpotRating.setUser_ID(scenicSpotComment.getId());  //设置 用户id
        userScenicSpotRating.setScenicspot_ID(ScenicSpotId);  //设置 景点id
        userScenicSpotRating.setComment(scenicSpotComment.getComment());  //设置 评论内容
        userScenicSpotRating.setScore(scenicSpotComment.getScore()); //设置 评论分数
        userScenicSpotRating.setId(String.valueOf(UUID.randomUUID())); //FIXME 强行设置id为UUID,原因是id为NULL插入后不知道为什么成为了Long的自增，怀疑是底层BUG
        userScenicSpotRatingMapper.insert(userScenicSpotRating);  //向数据库中插入评论
        String commentId = userScenicSpotRating.getId(); //获取评论id
        //上面是插入评论内容，下面是插入景点评论的图片
        if(CommentImages!=null&& CommentImages.length!=0) {
            List<scenicspot_comment_image> scenicspotCommentImageList = new ArrayList<>();
            for (MultipartFile file : CommentImages) {
                scenicspot_comment_image scenicSpotCommentImage = new scenicspot_comment_image();
                scenicSpotCommentImage.setComment_ID(commentId);
                scenicSpotCommentImage.setImage(ImageUtil.fileUpLoad(file, scenicSpotCommentImageLocation));  //保存图片
                scenicspotCommentImageList.add(scenicSpotCommentImage);
            }
            scenicSpotCommentImageMapper.batchInsertScenicSpotCommentImages(scenicspotCommentImageList);  //向数据库中插入图片数据
        }
    }


    //TODO 评论不允许修改，只允许删除，所以没有 PUT 的相关控制方法


    /**
     * 根据评论的id值删除评论以及对应的图片
     * @param id 评论的id值
     */
    @DeleteMapping("/scenicSpotComment/{id}")
    public void deleteScenicSpotCommentById(@PathVariable String id){
        QueryWrapper<scenicspot_comment_image> queryWrapperOfImages = new QueryWrapper<>();
        queryWrapperOfImages.eq("comment_ID",id);
        List<scenicspot_comment_image> scenicspotCommentImageList = scenicSpotCommentImageMapper.selectList(queryWrapperOfImages);  //获取对应comment_ID的数据列表
        for(scenicspot_comment_image scenicspotCommentImage : scenicspotCommentImageList){
            ImageUtil.deleteFile(scenicSpotCommentImageLocation + scenicspotCommentImage.getImage());  //先删除存储的图片
        }
        scenicSpotCommentImageMapper.delete(queryWrapperOfImages);  //在删除数据库中存储的图片信息
        userScenicSpotRatingMapper.deleteById(id);  //根据主键id删除评论信息
    }
}
