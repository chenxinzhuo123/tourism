package com.example.tourism.Controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.tourism.Mapper.route.route_scenicspotMapper;
import com.example.tourism.Mapper.route.routeMapper;
import com.example.tourism.bean.route.route;
import com.example.tourism.bean.route.route_scenicspot;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class routeController { //TODO 单条路线的CRUD以及审核控制
    @Autowired
    private route_scenicspotMapper routeScenicSpotMapper;
    @Autowired
    private routeMapper routeMapper;
    private static final String pass = "通过";
    private static final String fail = "未通过";
    private static final String audit = "待审核";

    /**
     * 根据id查询单条路线的详情
     * @param id 路线的id值
     * @return 返回route和List/<scenicspot>。具体内容查询两个类
     */
    @GetMapping("/singleRoute/{id}")
    public Map<String,Object> getSingleRouteById(@PathVariable long id){
        Map<String,Object> map = new HashMap<>();
        route route = routeMapper.selectById(id);

        QueryWrapper<route_scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("cnt");   //按照cnt的升序进行排列
        queryWrapper.eq("routes_ID",id);  //路线id值设置
        List<route_scenicspot> routeScenicspotList = routeScenicSpotMapper.selectList(queryWrapper);

        map.put("route",route);
        map.put("scenicSpots",routeScenicspotList);
        return map;
    }

    /**
     * 插入单条路线信息
     * 介绍路线信息包括内容
     * name 路线名
     * description 路线描述 非必须
     * 介绍单个景点包括内容
     * spotName 景点名
     * spot_Lat_Lon 景点的经纬度，先纬度后经度，记得经纬度检验是否符合标准
     * @param routeJson 路线信息
     * @param scenicSpotsJsonArr 路线对应的景点信息
     * @throws JsonProcessingException
     */
    @PostMapping("/singleRoute")
    public void insertSingleRoute(@RequestParam("route") String routeJson,
                                  @RequestParam("scenicSpots") String[] scenicSpotsJsonArr) throws JsonProcessingException {
        route route = new ObjectMapper().readValue(routeJson,route.class);
        route.setStatus(audit);  //路线初始状态为 待审核
        route.setCreateuser(1L);
        routeMapper.insert(route);  //插入路线信息
        long routeId = route.getId();

        List<route_scenicspot> routeScenicspotList = new ArrayList<>();
        short cnt = 0;  //位次，从0开始，上限为9，即一共最多10个
        for(String scenicSpotJson : scenicSpotsJsonArr){
            route_scenicspot routeScenicSpot = new ObjectMapper().readValue(scenicSpotJson,route_scenicspot.class);
            routeScenicSpot.setCnt(cnt++);   //设置位次
            routeScenicSpot.setRoutes_ID(routeId);  //设置id值
            routeScenicspotList.add(routeScenicSpot);
        }
        routeScenicSpotMapper.batchInsertScenicSpotForRoute(routeScenicspotList);  //插入路线对应的景点信息，或者是地点信息
    }

    /**
     * 更新单条路线信息
     * 介绍路线信息包括内容
     * id 路线id
     * name 路线名
     * description 路线描述 非必须
     * 介绍单个景点包括内容
     * spotName 景点名
     * spot_Lat_Lon 景点的经纬度，先纬度后经度，记得经纬度检验是否符合标准
     * @param routeJson 路线信息
     * @param scenicSpotsJsonArr 路线对应的景点信息
     * @throws JsonProcessingException
     */
    @PutMapping("/singleRoute")
    public void updateSingleRoute(@RequestParam("route") String routeJson,
                                  @RequestParam("scenicSpots") String[] scenicSpotsJsonArr) throws JsonProcessingException {
        route route = new ObjectMapper().readValue(routeJson,route.class);
        route.setStatus(audit);  //路线初始状态为 待审核
        routeMapper.updateById(route);  //更新路线信息
        long routeId = route.getId();
        //先删除对应的景点信息
        QueryWrapper<route_scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("routes_ID",routeId);  //路线id值设置
        routeScenicSpotMapper.delete(queryWrapper);  //删除路线对应的景点
        //添加新的景点信息
        List<route_scenicspot> routeScenicspotList = new ArrayList<>();
        short cnt = 0;  //位次，从0开始，上限为9，即一共最多10个
        for(String scenicSpotJson : scenicSpotsJsonArr){
            route_scenicspot routeScenicSpot = new ObjectMapper().readValue(scenicSpotJson,route_scenicspot.class);
            routeScenicSpot.setCnt(cnt++);   //设置位次
            routeScenicSpot.setRoutes_ID(routeId);  //设置id值
            routeScenicspotList.add(routeScenicSpot);
        }
        routeScenicSpotMapper.batchInsertScenicSpotForRoute(routeScenicspotList);  //插入路线对应的景点信息，或者是地点信息
    }

    /**
     * 根据id将路线状态设置为通过
     * @param id 路线id值
     */
    @PutMapping("/singleRoute/pass/{id}")
    public void updateSingleRouteForPass(@PathVariable long id){
        routeMapper.updateStatus(id,pass);
    }
    /**
     * 根据id将路线状态设置为未通过
     * @param id 路线id值
     */
    @PutMapping("/singleRoute/fail/{id}")
    public void updateSingleRouteForFail(@PathVariable long id){
        routeMapper.updateStatus(id,fail);
    }
    /**
     * 根据id将路线状态设置为待审核
     * @param id 路线id值
     */
    @PutMapping("/singleRoute/audit/{id}")
    public void updateSingleRouteForAudit(@PathVariable long id){
        routeMapper.updateStatus(id,audit);
    }
    /**
     * 根据id删除单个路线
     * @param id 路线的id值
     */
    @DeleteMapping("/singleRoute/{id}")
    public void DeleteSingleRouteById(@PathVariable long id){
        QueryWrapper<route_scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("routes_ID",id);  //路线id值设置
        routeScenicSpotMapper.delete(queryWrapper);  //删除路线对应的景点

        routeMapper.deleteById(id);  //删除路线
    }
}
