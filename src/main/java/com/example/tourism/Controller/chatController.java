package com.example.tourism.Controller;

import com.example.tourism.Service.ERNIE_Bot_4Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class chatController {  // TODO 对话控制器，用于控制用户与千帆大模型的对话功能，暂时命名为JourneyElf，旅途精灵
    @Autowired
    private ERNIE_Bot_4Service ernieBot4Service;

    /**
     * 获取千帆大模型对话
     * 下面讲述map返回值内容
     * status  int。1表示有正常返回。 2表示模型生成文字超时，建议重试。 3表示有底层bug或没有额度，需要开发人员解决。 以下参数都是status为1清空下才有
     * id  String 本轮对话的id
     * is_truncated  boolean 当前生成的结果是否被截断
     * result  String 对话返回结果
     * need_clear_history  boolean 表示用户输入是否存在安全，是否关闭当前会话，清理历史会话信息
     * ban_round  int 当need_clear_history为true时，此字段会告知第几轮对话有敏感信息，如果是当前问题，ban_round=-1，如果为false，则没有此字段
     * @param userId 用户id值
     * @param content 此次对话内容，用户提问的内容，不能超过3000字符
     * @return map封装的返回值
     */
    @PostMapping("/chatWithJourneyElf")
    public Map<String,Object> chatWithJourneyElf(@RequestParam("userId") long userId, @RequestParam("content") String content) {
        try {
            return ernieBot4Service.getResponse(userId,content);
        }
        catch (SocketTimeoutException e){
            Map<String,Object> map = new HashMap<>();
            map.put("status",2);
            return map;
        }
        catch (Exception e){
            Map<String,Object> map = new HashMap<>();
            map.put("status",3);
            return map;
        }
    }
}
