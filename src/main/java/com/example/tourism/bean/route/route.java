package com.example.tourism.bean.route;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class route {
    private Long id;
    private String name;
    private String description;
    private Float averageRating;
    private Long createuser;
    private Date createtime;
    private String status;
}
