package com.example.tourism.bean.route;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class user_route_rating {
    private String id;
    private Long user_ID;
    private Long routes_ID;
    private Float score;
    private String comment;
    private Date date;
}
