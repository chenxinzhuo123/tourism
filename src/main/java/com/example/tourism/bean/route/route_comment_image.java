package com.example.tourism.bean.route;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class route_comment_image {
    private String id;
    private String comment_ID;
    private String image;
}
