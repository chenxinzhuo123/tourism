package com.example.tourism.bean.route;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class route_scenicspot {
    private String id;
    private Long routes_ID;  //路线的id值
    private String spotName;  //景点名字
    private String address; //地点位置，由api得到
    private String spot_Lat_Lon;  //景点坐标
    private Short cnt;  //位于路线第几个
}
