package com.example.tourism.bean.route;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class routeComment {
    private Long id; //用户id
    private String name;  //用户名
    private String image; //用户头像
    private String commentID; //评论的id
    private String comment; //评论内容
    private Float score; //评分
    private Date time; //评论时间
}
