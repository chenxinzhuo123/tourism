package com.example.tourism.bean.conversation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class message {
    private String id;
    private String conversation_ID;
    private Long sender_ID;
    private Long receiver_ID;
    private String attachment;  //附件名字
}
