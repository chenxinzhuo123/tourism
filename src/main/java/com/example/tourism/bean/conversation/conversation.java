package com.example.tourism.bean.conversation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class conversation {
    private String id;
    private Long userA;
    private Long userB;
    private Date lastMessage;
}
