package com.example.tourism.bean.scenicspot;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class scenicspot_comment_image {
    private String id;
    private String comment_ID;
    private String image;
}
