package com.example.tourism.bean.scenicspot;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class user_scenicspot_rating {
    private String id;
    private Long user_ID;
    private Long scenicspot_ID;
    private Float score;
    private String comment;
    private Date date;
}
