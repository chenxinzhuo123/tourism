package com.example.tourism.bean.scenicspot;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class scenicspot {
    private Long id;
    private String name;
    private String description;
    private String location;
    private String type;
    private Float averageRating;
    private Long createuser;
    private Date createtime;
    private String status;
    private String lon_lat;  //先纬度再经度
    private String belong;
    private Integer visit_time;  //预计的游览时间，单位分钟

}
