package com.example.tourism.bean.scenicspot;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class scenicspot_image {
    private String id;
    private Long attraction_ID;
    private String image;
}
