package com.example.tourism.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

@TableName("user")
public class user {
    private Long id;
    private String name;
    private String tel;
    private String password;
    private String sex;
    private String email;
    private Date registerTime;
    private Date birth;
    private String self_introduction;
    private String type;
    private String image; //头像
}
