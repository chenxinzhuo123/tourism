package com.example.tourism.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class locationSearchReturn {
    private String name;  //地点名
    private String location; //坐标
    private String address; //位置，地址
}
