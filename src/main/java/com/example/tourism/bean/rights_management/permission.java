package com.example.tourism.bean.rights_management;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class permission {
    private String id;
    private String name;
    private String URL;
    private String remark;
}
