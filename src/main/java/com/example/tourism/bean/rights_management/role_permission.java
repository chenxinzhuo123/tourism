package com.example.tourism.bean.rights_management;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class role_permission {
    private Integer id;
    private String roleId;
    private String permissionId;
    private String remark;
}
