package com.example.tourism.Mapper.conversation;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.conversation.conversation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface conversationMapper extends BaseMapper<conversation> {
}
