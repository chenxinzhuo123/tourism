package com.example.tourism.Mapper.conversation;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.conversation.message;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface messageMapper extends BaseMapper<message> {
}
