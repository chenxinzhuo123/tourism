package com.example.tourism.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.user;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
//可以不写@Mapper但使用@Autowired时会爆红，不影响使用但不好看
public interface userMapper extends BaseMapper<user>{

}
