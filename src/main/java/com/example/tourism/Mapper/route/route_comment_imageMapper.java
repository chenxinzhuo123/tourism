package com.example.tourism.Mapper.route;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.route.route_comment_image;
import com.example.tourism.bean.scenicspot.scenicspot_comment_image;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface route_comment_imageMapper extends BaseMapper<route_comment_image> {
    @Insert("<script>INSERT INTO route_comment_image(comment_ID, image) VALUES " +
            "<foreach collection='list' item='item' separator=','>" +
            "(#{item.comment_ID}, #{item.image})" +
            "</foreach></script>")
    void batchInsertRouteCommentImages(@Param("list") List<route_comment_image> images);  //批量插入 景点评论 的 图片
}
