package com.example.tourism.Mapper.route;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.route.route_scenicspot;
import com.example.tourism.bean.scenicspot.scenicspot_image;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface route_scenicspotMapper extends BaseMapper<route_scenicspot> {
    @Insert("<script>INSERT INTO route_scenicspot(routes_ID, spotName, address , spot_Lat_Lon, cnt) VALUES " +
            "<foreach collection='list' item='item' separator=','>" +
            "(#{item.routes_ID}, #{item.spotName} , #{item.address}, #{item.spot_Lat_Lon}, #{item.cnt})" +
            "</foreach></script>")
    public void batchInsertScenicSpotForRoute(@Param("list") List<route_scenicspot> routeScenicspotList);  //批量插入路线对应的景点


}
