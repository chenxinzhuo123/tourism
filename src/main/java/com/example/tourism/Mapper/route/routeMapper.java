package com.example.tourism.Mapper.route;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.route.route;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface routeMapper extends BaseMapper<route> {
    @Update("update route set status=#{status} where id=#{id}")
    public void updateStatus(@Param("id") long id, @Param("status") String status);  //更新路线状态
}
