package com.example.tourism.Mapper.route;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.route.routeComment;
import com.example.tourism.bean.route.user_route_rating;
import com.example.tourism.bean.scenicspot.scenicSpotComment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface user_route_ratingMapper extends BaseMapper<user_route_rating> {
    @Select("select user.id,name,image,user_route_rating.id as commentID,comment,score,time " +
            "from user , user_route_rating " +
            "where user.id = user_route_rating.user_ID and user_route_rating.route_ID = #{route_ID};")
    public List<routeComment> getRouteCommentByRouteId(@Param("route_ID") long route_ID);
}
