package com.example.tourism.Mapper.scenicspot;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.scenicspot.scenicSpotComment;
import com.example.tourism.bean.scenicspot.user_scenicspot_rating;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface user_scenicspot_ratingMapper extends BaseMapper<user_scenicspot_rating> {
    @Select("select user.id,name,image,user_scenicspot_rating.id as commentID,comment,score,time " +
            "from user , user_scenicspot_rating " +
            "where user.id = user_scenicspot_rating.user_ID and user_scenicspot_rating.scenicspot_ID = #{scenicspot_ID};")
    public List<scenicSpotComment> getScenicSpotCommentByScenicSpotId(@Param("scenicspot_ID") long scenicspot_ID);
}
