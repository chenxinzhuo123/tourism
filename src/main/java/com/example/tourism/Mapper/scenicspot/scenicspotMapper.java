package com.example.tourism.Mapper.scenicspot;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.scenicspot.scenicspot;
import com.example.tourism.bean.scenicspot.scenicspotWithComment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface scenicspotMapper extends BaseMapper<scenicspot> {
    @Select("SELECT scenicspot.*,us.user_ID,us.score,us.comment\n" +
            "            FROM scenicspot\n" +
            "            left join(\n" +
            "            select scenicspot_ID,user_ID,score,comment \n" +
            "            from user_scenicspot_rating\n" +
            "            where scenicspot_ID in (select id from scenicspot)) as us \n" +
            "            on us.scenicspot_ID=scenicspot.id;")

    List<scenicspotWithComment> getAll();
    
    @Select("select distinct belong " +
            "from scenicspot")
    public List<String> getBelongs();

    @Update("update scenicspot set status=#{status} where id=#{id}")
    public void updateScenicSpotStatus(@Param("id") long id,@Param("status") String status);
}
