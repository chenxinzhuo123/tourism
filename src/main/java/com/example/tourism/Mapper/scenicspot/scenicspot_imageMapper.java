package com.example.tourism.Mapper.scenicspot;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.scenicspot.scenicspot_image;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface scenicspot_imageMapper extends BaseMapper<scenicspot_image> {
    @Select("SELECT id, attraction_ID, image " +
            "FROM ( " +
            "    SELECT " +
            "        id, " +
            "        attraction_ID, " +
            "        image, " +
            "        ROW_NUMBER() OVER(PARTITION BY attraction_ID ORDER BY id) AS rn " +
            "    FROM scenicspot_image " +
            ") AS ranked " +
            "WHERE rn = 1; ")
    public List<scenicspot_image> getSingleImg();

    @Select("<script> " +
            "SELECT id, attraction_ID, image " +
            "FROM ( " +
            "   SELECT id, attraction_ID, image, " +
            "       ROW_NUMBER() OVER(PARTITION BY attraction_ID ORDER BY id) AS rn " +
            "   FROM scenicspot_image " +
            "   WHERE attraction_ID IN " +
            "       <foreach collection='attractionIdList' item='attractionId' open='(' separator=',' close=')'> " +
            "           #{attractionId} " +
            "       </foreach> " +
            ") AS ranked " +
            "WHERE rn = 1 " +
            "ORDER BY id; " +
            "</script> ")
    public List<scenicspot_image> getSingleImgByIds(@Param("attractionIdList") List<Long> attractionIdList);   //根据idList查询景点图片

    @Insert("<script>INSERT INTO scenicspot_image(attraction_ID, image) VALUES " +
            "<foreach collection='list' item='item' separator=','>" +
            "(#{item.attraction_ID}, #{item.image})" +
            "</foreach></script>")
    void batchInsertScenicSpotImages(@Param("list") List<scenicspot_image> images);  //批量插入景点图片

}
