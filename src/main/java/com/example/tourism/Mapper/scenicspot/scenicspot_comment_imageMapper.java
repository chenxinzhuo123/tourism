package com.example.tourism.Mapper.scenicspot;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.scenicspot.scenicspot_comment_image;
import com.example.tourism.bean.scenicspot.scenicspot_image;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface scenicspot_comment_imageMapper extends BaseMapper<scenicspot_comment_image> {
    @Insert("<script>INSERT INTO scenicspot_comment_image(comment_ID, image) VALUES " +
            "<foreach collection='list' item='item' separator=','>" +
            "(#{item.comment_ID}, #{item.image})" +
            "</foreach></script>")
    void batchInsertScenicSpotCommentImages(@Param("list") List<scenicspot_comment_image> images);  //批量插入 景点评论 的 图片
}
