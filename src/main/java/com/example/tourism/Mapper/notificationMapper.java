package com.example.tourism.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.notification;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface notificationMapper extends BaseMapper<notification> {
}
