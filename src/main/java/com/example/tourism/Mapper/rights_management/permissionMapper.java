package com.example.tourism.Mapper.rights_management;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tourism.bean.rights_management.permission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface permissionMapper extends BaseMapper<permission> {
}
