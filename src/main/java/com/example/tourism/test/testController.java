package com.example.tourism.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.tourism.Mapper.scenicspot.scenicspotMapper;
import com.example.tourism.bean.locationSearchReturn;
import com.example.tourism.bean.scenicspot.scenicspot;
import com.example.tourism.utils.API.locationSearch_API;
import com.example.tourism.utils.API.walk_API;
import com.example.tourism.utils.routing;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class testController {
    @Value("${tourism.app.ak}")
    private String AK ;
    @Autowired
    private scenicspotMapper spotsMapper;
    @GetMapping("/test")
    public void get() throws Exception {
        routing r = new routing("40.45,116.34|40.54,116.35|40.34,116.45|40.35,116.46","13","0",AK,4);
        r.run();

    }

    @GetMapping("/testHTML")
    public String getTestHTML(){
        return "testHTML";
    }

    @PostMapping("/locationSearch")
    @ResponseBody
    public List<locationSearchReturn> locationSearch(@RequestParam("keyword") String keyword, @RequestParam("region") String region, @RequestParam("location") String location3) throws Exception {
        List<locationSearchReturn> locationSearchReturnList = new ArrayList<>();
        locationSearch_API api = new locationSearch_API(AK,keyword,region,location3);
        String str = api.getResponse();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(str);
            int status = rootNode.get("status").asInt();
            if (status == 0) {
                JsonNode results = rootNode.get("result");
                if (results.isArray()) {
                    for (JsonNode result : results) {
                        String name = result.get("name").asText();
                        JsonNode location = result.get("location");
                        String address = result.get("address").asText();
                        locationSearchReturn locationSearchReturn = new locationSearchReturn();
                        locationSearchReturn.setName(name);
                        locationSearchReturn.setLocation(location.get("lat").asText()+","+location.get("lng").asText());
                        locationSearchReturn.setAddress(address);
                        locationSearchReturnList.add(locationSearchReturn);
                        // 在这里可以将数据存储到一个数组或列表中
                    }
                }
//                System.out.println("ok");
            }
            else {
//                System.out.println(status);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationSearchReturnList;
    }
    @GetMapping("/testForRouting")
    public void getSpots(){
        List<Long> idList = new ArrayList<>();
//        idList.add(1L);
//        idList.add(5L);
//        idList.add(4L);
//        idList.add(3L);

        idList.add(1L);
        idList.add(3L);
        idList.add(4L);
        idList.add(5L);

        QueryWrapper<scenicspot> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id", idList); // 在这里，"id" 是你的实体类对应数据库表中的 ID 列名
        String[] idArray = new String[idList.size()];
        for(int i=0;i<idList.size();++i){
            idArray[i] = String.valueOf(idList.get(i));
        }
        String orderByClause = "FIELD(id, " + String.join(",",idArray) + ")";
        queryWrapper.orderByAsc(orderByClause);
        List<scenicspot> spotsList = spotsMapper.selectList(queryWrapper); //获取id在idList中的数据
        System.out.println(String.join(",",idArray));
        System.out.println(idList);
        for(scenicspot s : spotsList){
            System.out.print("--->"+s.getId());
        }
        System.out.println();
    }
}
